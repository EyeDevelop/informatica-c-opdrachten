﻿namespace Opdrachten
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_calculator = new System.Windows.Forms.Button();
            this.btn_valuta = new System.Windows.Forms.Button();
            this.btn_circle = new System.Windows.Forms.Button();
            this.btn_repeat = new System.Windows.Forms.Button();
            this.btn_texttools = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_calculator
            // 
            this.btn_calculator.Location = new System.Drawing.Point(12, 12);
            this.btn_calculator.Name = "btn_calculator";
            this.btn_calculator.Size = new System.Drawing.Size(111, 23);
            this.btn_calculator.TabIndex = 0;
            this.btn_calculator.Text = "Calculator";
            this.btn_calculator.UseVisualStyleBackColor = true;
            this.btn_calculator.Click += new System.EventHandler(this.btn_calculator_Click);
            // 
            // btn_valuta
            // 
            this.btn_valuta.Location = new System.Drawing.Point(161, 12);
            this.btn_valuta.Name = "btn_valuta";
            this.btn_valuta.Size = new System.Drawing.Size(111, 23);
            this.btn_valuta.TabIndex = 1;
            this.btn_valuta.Text = "Valuta";
            this.btn_valuta.UseVisualStyleBackColor = true;
            this.btn_valuta.Click += new System.EventHandler(this.btn_valuta_Click);
            // 
            // btn_circle
            // 
            this.btn_circle.Location = new System.Drawing.Point(12, 41);
            this.btn_circle.Name = "btn_circle";
            this.btn_circle.Size = new System.Drawing.Size(111, 23);
            this.btn_circle.TabIndex = 2;
            this.btn_circle.Text = "Circle area";
            this.btn_circle.UseVisualStyleBackColor = true;
            this.btn_circle.Click += new System.EventHandler(this.btn_circle_Click);
            // 
            // btn_repeat
            // 
            this.btn_repeat.Location = new System.Drawing.Point(161, 41);
            this.btn_repeat.Name = "btn_repeat";
            this.btn_repeat.Size = new System.Drawing.Size(111, 23);
            this.btn_repeat.TabIndex = 3;
            this.btn_repeat.Text = "Text repeat";
            this.btn_repeat.UseVisualStyleBackColor = true;
            this.btn_repeat.Click += new System.EventHandler(this.btn_repeat_Click);
            // 
            // btn_texttools
            // 
            this.btn_texttools.Location = new System.Drawing.Point(12, 70);
            this.btn_texttools.Name = "btn_texttools";
            this.btn_texttools.Size = new System.Drawing.Size(111, 23);
            this.btn_texttools.TabIndex = 4;
            this.btn_texttools.Text = "TextTools";
            this.btn_texttools.UseVisualStyleBackColor = true;
            this.btn_texttools.Click += new System.EventHandler(this.btn_texttools_Click);
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 106);
            this.Controls.Add(this.btn_texttools);
            this.Controls.Add(this.btn_repeat);
            this.Controls.Add(this.btn_circle);
            this.Controls.Add(this.btn_valuta);
            this.Controls.Add(this.btn_calculator);
            this.Name = "ControlPanel";
            this.Text = "ControlPanel";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_calculator;
        private System.Windows.Forms.Button btn_valuta;
        private System.Windows.Forms.Button btn_circle;
        private System.Windows.Forms.Button btn_repeat;
        private System.Windows.Forms.Button btn_texttools;
    }
}