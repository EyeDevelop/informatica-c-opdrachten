﻿using System;
using System.Windows.Forms;

namespace Opdrachten
{
    public partial class ControlPanel : Form
    {
        public ControlPanel()
        {
            InitializeComponent();
        }

        private void btn_calculator_Click(object sender, EventArgs e)
        {
            new Rekenmachine().Show();
        }

        private void btn_valuta_Click(object sender, EventArgs e)
        {
            new Valuta().Show();
        }

        private void btn_circle_Click(object sender, EventArgs e)
        {
            new Circle().Show();
        }

        private void btn_repeat_Click(object sender, EventArgs e)
        {
            new Repeat().Show();
        }

        private void btn_texttools_Click(object sender, EventArgs e)
        {
            new TextTools().Show();
        }
    }
}
