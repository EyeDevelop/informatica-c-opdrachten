﻿namespace Opdrachten
{
    partial class TextTools
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_last = new System.Windows.Forms.TextBox();
            this.txt_first = new System.Windows.Forms.TextBox();
            this.txt_text = new System.Windows.Forms.TextBox();
            this.lbl_text = new System.Windows.Forms.Label();
            this.lbl_first = new System.Windows.Forms.Label();
            this.lbl_last = new System.Windows.Forms.Label();
            this.lbl_first_ascii = new System.Windows.Forms.Label();
            this.lbl_last_ascii = new System.Windows.Forms.Label();
            this.txt_last_ascii = new System.Windows.Forms.TextBox();
            this.txt_first_ascii = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_last
            // 
            this.txt_last.Location = new System.Drawing.Point(15, 162);
            this.txt_last.Name = "txt_last";
            this.txt_last.Size = new System.Drawing.Size(100, 20);
            this.txt_last.TabIndex = 1;
            // 
            // txt_first
            // 
            this.txt_first.Location = new System.Drawing.Point(15, 123);
            this.txt_first.Name = "txt_first";
            this.txt_first.Size = new System.Drawing.Size(100, 20);
            this.txt_first.TabIndex = 2;
            // 
            // txt_text
            // 
            this.txt_text.Location = new System.Drawing.Point(15, 25);
            this.txt_text.Name = "txt_text";
            this.txt_text.Size = new System.Drawing.Size(257, 20);
            this.txt_text.TabIndex = 3;
            this.txt_text.TextChanged += new System.EventHandler(this.txt_text_TextChanged);
            // 
            // lbl_text
            // 
            this.lbl_text.AutoSize = true;
            this.lbl_text.Location = new System.Drawing.Point(12, 9);
            this.lbl_text.Name = "lbl_text";
            this.lbl_text.Size = new System.Drawing.Size(117, 13);
            this.lbl_text.TabIndex = 4;
            this.lbl_text.Text = "Please enter some text:";
            // 
            // lbl_first
            // 
            this.lbl_first.AutoSize = true;
            this.lbl_first.Location = new System.Drawing.Point(12, 107);
            this.lbl_first.Name = "lbl_first";
            this.lbl_first.Size = new System.Drawing.Size(55, 13);
            this.lbl_first.TabIndex = 5;
            this.lbl_first.Text = "First letter:";
            // 
            // lbl_last
            // 
            this.lbl_last.AutoSize = true;
            this.lbl_last.Location = new System.Drawing.Point(12, 146);
            this.lbl_last.Name = "lbl_last";
            this.lbl_last.Size = new System.Drawing.Size(56, 13);
            this.lbl_last.TabIndex = 6;
            this.lbl_last.Text = "Last letter:";
            // 
            // lbl_first_ascii
            // 
            this.lbl_first_ascii.AutoSize = true;
            this.lbl_first_ascii.Location = new System.Drawing.Point(169, 107);
            this.lbl_first_ascii.Name = "lbl_first_ascii";
            this.lbl_first_ascii.Size = new System.Drawing.Size(64, 13);
            this.lbl_first_ascii.TabIndex = 7;
            this.lbl_first_ascii.Text = "ASCII code:";
            // 
            // lbl_last_ascii
            // 
            this.lbl_last_ascii.AutoSize = true;
            this.lbl_last_ascii.Location = new System.Drawing.Point(169, 146);
            this.lbl_last_ascii.Name = "lbl_last_ascii";
            this.lbl_last_ascii.Size = new System.Drawing.Size(64, 13);
            this.lbl_last_ascii.TabIndex = 8;
            this.lbl_last_ascii.Text = "ASCII code:";
            // 
            // txt_last_ascii
            // 
            this.txt_last_ascii.Location = new System.Drawing.Point(172, 162);
            this.txt_last_ascii.Name = "txt_last_ascii";
            this.txt_last_ascii.Size = new System.Drawing.Size(100, 20);
            this.txt_last_ascii.TabIndex = 9;
            // 
            // txt_first_ascii
            // 
            this.txt_first_ascii.Location = new System.Drawing.Point(172, 123);
            this.txt_first_ascii.Name = "txt_first_ascii";
            this.txt_first_ascii.Size = new System.Drawing.Size(100, 20);
            this.txt_first_ascii.TabIndex = 10;
            // 
            // TextTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 198);
            this.Controls.Add(this.txt_first_ascii);
            this.Controls.Add(this.txt_last_ascii);
            this.Controls.Add(this.lbl_last_ascii);
            this.Controls.Add(this.lbl_first_ascii);
            this.Controls.Add(this.lbl_last);
            this.Controls.Add(this.lbl_first);
            this.Controls.Add(this.lbl_text);
            this.Controls.Add(this.txt_text);
            this.Controls.Add(this.txt_first);
            this.Controls.Add(this.txt_last);
            this.Name = "TextTools";
            this.Text = "TextTools";
            this.Load += new System.EventHandler(this.TextTools_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txt_last;
        private System.Windows.Forms.TextBox txt_first;
        private System.Windows.Forms.TextBox txt_text;
        private System.Windows.Forms.Label lbl_text;
        private System.Windows.Forms.Label lbl_first;
        private System.Windows.Forms.Label lbl_last;
        private System.Windows.Forms.Label lbl_first_ascii;
        private System.Windows.Forms.Label lbl_last_ascii;
        private System.Windows.Forms.TextBox txt_last_ascii;
        private System.Windows.Forms.TextBox txt_first_ascii;
    }
}