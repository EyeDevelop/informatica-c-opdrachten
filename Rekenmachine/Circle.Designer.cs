﻿namespace Opdrachten
{
    partial class Circle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_diameter = new System.Windows.Forms.TextBox();
            this.txt_radius = new System.Windows.Forms.TextBox();
            this.txt_circumference = new System.Windows.Forms.TextBox();
            this.txt_area = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.lbl_radius = new System.Windows.Forms.Label();
            this.lbl_diameter = new System.Windows.Forms.Label();
            this.lbl_area = new System.Windows.Forms.Label();
            this.lbl_circumference = new System.Windows.Forms.Label();
            this.lbl_err = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_diameter
            // 
            this.txt_diameter.Location = new System.Drawing.Point(12, 98);
            this.txt_diameter.Name = "txt_diameter";
            this.txt_diameter.Size = new System.Drawing.Size(198, 20);
            this.txt_diameter.TabIndex = 0;
            // 
            // txt_radius
            // 
            this.txt_radius.Location = new System.Drawing.Point(12, 25);
            this.txt_radius.Name = "txt_radius";
            this.txt_radius.Size = new System.Drawing.Size(198, 20);
            this.txt_radius.TabIndex = 1;
            // 
            // txt_circumference
            // 
            this.txt_circumference.Location = new System.Drawing.Point(12, 176);
            this.txt_circumference.Name = "txt_circumference";
            this.txt_circumference.Size = new System.Drawing.Size(198, 20);
            this.txt_circumference.TabIndex = 2;
            // 
            // txt_area
            // 
            this.txt_area.Location = new System.Drawing.Point(12, 137);
            this.txt_area.Name = "txt_area";
            this.txt_area.Size = new System.Drawing.Size(198, 20);
            this.txt_area.TabIndex = 3;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(12, 332);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(198, 23);
            this.btn_calc.TabIndex = 4;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // lbl_radius
            // 
            this.lbl_radius.AutoSize = true;
            this.lbl_radius.Location = new System.Drawing.Point(9, 9);
            this.lbl_radius.Name = "lbl_radius";
            this.lbl_radius.Size = new System.Drawing.Size(43, 13);
            this.lbl_radius.TabIndex = 5;
            this.lbl_radius.Text = "Radius:";
            // 
            // lbl_diameter
            // 
            this.lbl_diameter.AutoSize = true;
            this.lbl_diameter.Location = new System.Drawing.Point(9, 82);
            this.lbl_diameter.Name = "lbl_diameter";
            this.lbl_diameter.Size = new System.Drawing.Size(52, 13);
            this.lbl_diameter.TabIndex = 6;
            this.lbl_diameter.Text = "Diameter:";
            // 
            // lbl_area
            // 
            this.lbl_area.AutoSize = true;
            this.lbl_area.Location = new System.Drawing.Point(9, 121);
            this.lbl_area.Name = "lbl_area";
            this.lbl_area.Size = new System.Drawing.Size(71, 13);
            this.lbl_area.TabIndex = 7;
            this.lbl_area.Text = "Surface area:";
            // 
            // lbl_circumference
            // 
            this.lbl_circumference.AutoSize = true;
            this.lbl_circumference.Location = new System.Drawing.Point(9, 160);
            this.lbl_circumference.Name = "lbl_circumference";
            this.lbl_circumference.Size = new System.Drawing.Size(78, 13);
            this.lbl_circumference.TabIndex = 8;
            this.lbl_circumference.Text = "Circumference:";
            // 
            // lbl_err
            // 
            this.lbl_err.AutoSize = true;
            this.lbl_err.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_err.Location = new System.Drawing.Point(9, 272);
            this.lbl_err.Name = "lbl_err";
            this.lbl_err.Size = new System.Drawing.Size(0, 13);
            this.lbl_err.TabIndex = 9;
            // 
            // Circle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(222, 367);
            this.Controls.Add(this.lbl_err);
            this.Controls.Add(this.lbl_circumference);
            this.Controls.Add(this.lbl_area);
            this.Controls.Add(this.lbl_diameter);
            this.Controls.Add(this.lbl_radius);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_area);
            this.Controls.Add(this.txt_circumference);
            this.Controls.Add(this.txt_radius);
            this.Controls.Add(this.txt_diameter);
            this.Name = "Circle";
            this.Text = "Circle";
            this.Load += new System.EventHandler(this.Circle_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_diameter;
        private System.Windows.Forms.TextBox txt_radius;
        private System.Windows.Forms.TextBox txt_circumference;
        private System.Windows.Forms.TextBox txt_area;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label lbl_radius;
        private System.Windows.Forms.Label lbl_diameter;
        private System.Windows.Forms.Label lbl_area;
        private System.Windows.Forms.Label lbl_circumference;
        private System.Windows.Forms.Label lbl_err;
    }
}