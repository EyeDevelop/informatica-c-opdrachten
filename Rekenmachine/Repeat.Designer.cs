﻿namespace Opdrachten
{
    partial class Repeat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_repeat = new System.Windows.Forms.TextBox();
            this.btn_show = new System.Windows.Forms.Button();
            this.lbl_repeat = new System.Windows.Forms.Label();
            this.lbl_err = new System.Windows.Forms.Label();
            this.lbl_count = new System.Windows.Forms.Label();
            this.txt_count = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_repeat
            // 
            this.txt_repeat.Location = new System.Drawing.Point(15, 25);
            this.txt_repeat.Name = "txt_repeat";
            this.txt_repeat.Size = new System.Drawing.Size(211, 20);
            this.txt_repeat.TabIndex = 0;
            // 
            // btn_show
            // 
            this.btn_show.Location = new System.Drawing.Point(12, 226);
            this.btn_show.Name = "btn_show";
            this.btn_show.Size = new System.Drawing.Size(214, 23);
            this.btn_show.TabIndex = 1;
            this.btn_show.Text = "Repeat";
            this.btn_show.UseVisualStyleBackColor = true;
            this.btn_show.Click += new System.EventHandler(this.btn_show_Click);
            // 
            // lbl_repeat
            // 
            this.lbl_repeat.AutoSize = true;
            this.lbl_repeat.Location = new System.Drawing.Point(12, 9);
            this.lbl_repeat.Name = "lbl_repeat";
            this.lbl_repeat.Size = new System.Drawing.Size(121, 13);
            this.lbl_repeat.TabIndex = 2;
            this.lbl_repeat.Text = "The text to be repeated:";
            // 
            // lbl_err
            // 
            this.lbl_err.AutoSize = true;
            this.lbl_err.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_err.Location = new System.Drawing.Point(12, 170);
            this.lbl_err.Name = "lbl_err";
            this.lbl_err.Size = new System.Drawing.Size(0, 13);
            this.lbl_err.TabIndex = 3;
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Location = new System.Drawing.Point(12, 48);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.Size = new System.Drawing.Size(178, 13);
            this.lbl_count.TabIndex = 4;
            this.lbl_count.Text = "The amount of times to be repeated:";
            // 
            // txt_count
            // 
            this.txt_count.Location = new System.Drawing.Point(15, 64);
            this.txt_count.Name = "txt_count";
            this.txt_count.Size = new System.Drawing.Size(211, 20);
            this.txt_count.TabIndex = 5;
            // 
            // Repeat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 261);
            this.Controls.Add(this.txt_count);
            this.Controls.Add(this.lbl_count);
            this.Controls.Add(this.lbl_err);
            this.Controls.Add(this.lbl_repeat);
            this.Controls.Add(this.btn_show);
            this.Controls.Add(this.txt_repeat);
            this.Name = "Repeat";
            this.Text = "Repeat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_repeat;
        private System.Windows.Forms.Button btn_show;
        private System.Windows.Forms.Label lbl_repeat;
        private System.Windows.Forms.Label lbl_err;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.TextBox txt_count;
    }
}