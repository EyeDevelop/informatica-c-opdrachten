﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Opdrachten
{
    public partial class TextTools : Form
    {
        public TextTools()
        {
            InitializeComponent();
        }

        private void TextTools_Load(object sender, EventArgs e)
        {
            txt_first.ReadOnly = true;
            txt_first_ascii.ReadOnly = true;

            txt_last.ReadOnly = true;
            txt_last_ascii.ReadOnly = true;
        }

        private void txt_text_TextChanged(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(txt_text.Text))
            {
                txt_first.Text = txt_text.Text[0].ToString();
                txt_first_ascii.Text = ((int)txt_text.Text[0]).ToString();

                txt_last.Text = txt_text.Text[txt_text.Text.Length - 1].ToString();
                txt_last_ascii.Text = ((int)txt_text.Text[txt_text.Text.Length - 1]).ToString();
            }
            else
            {
                txt_first.Text = "";
                txt_first_ascii.Text = "";

                txt_last.Text = "";
                txt_last_ascii.Text = "";
            }
        }
    }
}
