﻿using System;
using System.Windows.Forms;

namespace Opdrachten
{
    public partial class Rekenmachine : Form
    {
        public Rekenmachine()
        {
            InitializeComponent();
        }

        private void change_lbl(bool error)
        {
            if(error)
            {
                lbl_error.Text = "ERROR: One or more values are not a number";
            } else
            {
                lbl_error.Text = "";
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            double result = 0;
            try
            {
                result = double.Parse(txt_1.Text.Replace('.', ',')) + double.Parse(txt_2.Text.Replace('.', ','));
                change_lbl(false);
            } catch(Exception err)
            {
                change_lbl(true);
            }

            txt_result.Text = result.ToString();
        }

        private void btn_sub_Click(object sender, EventArgs e)
        {
            double result = 0;
            try
            {
                result = double.Parse(txt_1.Text.Replace('.', ',')) - double.Parse(txt_2.Text.Replace('.', ','));
                change_lbl(false);
            }
            catch (Exception err)
            {
                change_lbl(true);
            }

            txt_result.Text = result.ToString();
        }

        private void btn_mul_Click(object sender, EventArgs e)
        {
            double result = 0;
            try
            {
                result = double.Parse(txt_1.Text.Replace('.', ',')) * double.Parse(txt_2.Text.Replace('.', ','));
                change_lbl(false);
            }
            catch (Exception err)
            {
                change_lbl(true);
            }

            txt_result.Text = result.ToString();
        }

        private void btn_div_Click(object sender, EventArgs e)
        {
            double result = 0;
            try
            {
                result = double.Parse(txt_1.Text.Replace('.', ',')) / double.Parse(txt_2.Text.Replace('.', ','));
                change_lbl(false);
            }
            catch (Exception err)
            {
                change_lbl(true);
            }

            txt_result.Text = result.ToString();
        }
    }
}
