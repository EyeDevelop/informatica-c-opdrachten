﻿namespace Opdrachten
{
    partial class Valuta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_eur = new System.Windows.Forms.TextBox();
            this.txt_dol = new System.Windows.Forms.TextBox();
            this.btn_cusval = new System.Windows.Forms.Button();
            this.btn_calc = new System.Windows.Forms.Button();
            this.lbl_dol = new System.Windows.Forms.Label();
            this.lbl_eur = new System.Windows.Forms.Label();
            this.rb_eurdol = new System.Windows.Forms.RadioButton();
            this.rb_doleur = new System.Windows.Forms.RadioButton();
            this.lbl_err = new System.Windows.Forms.Label();
            this.txt_cusval = new System.Windows.Forms.TextBox();
            this.chk_cusval = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txt_eur
            // 
            this.txt_eur.Location = new System.Drawing.Point(12, 25);
            this.txt_eur.Name = "txt_eur";
            this.txt_eur.Size = new System.Drawing.Size(260, 20);
            this.txt_eur.TabIndex = 0;
            // 
            // txt_dol
            // 
            this.txt_dol.Location = new System.Drawing.Point(12, 76);
            this.txt_dol.Name = "txt_dol";
            this.txt_dol.Size = new System.Drawing.Size(260, 20);
            this.txt_dol.TabIndex = 1;
            // 
            // btn_cusval
            // 
            this.btn_cusval.Location = new System.Drawing.Point(153, 227);
            this.btn_cusval.Name = "btn_cusval";
            this.btn_cusval.Size = new System.Drawing.Size(119, 23);
            this.btn_cusval.TabIndex = 2;
            this.btn_cusval.Text = "Apply custom valuta";
            this.btn_cusval.UseVisualStyleBackColor = true;
            this.btn_cusval.Click += new System.EventHandler(this.btn_cusval_Click);
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(12, 227);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(93, 23);
            this.btn_calc.TabIndex = 3;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // lbl_dol
            // 
            this.lbl_dol.AutoSize = true;
            this.lbl_dol.Location = new System.Drawing.Point(9, 60);
            this.lbl_dol.Name = "lbl_dol";
            this.lbl_dol.Size = new System.Drawing.Size(88, 13);
            this.lbl_dol.TabIndex = 6;
            this.lbl_dol.Text = "Bedrag in dollars:";
            // 
            // lbl_eur
            // 
            this.lbl_eur.AutoSize = true;
            this.lbl_eur.Location = new System.Drawing.Point(9, 9);
            this.lbl_eur.Name = "lbl_eur";
            this.lbl_eur.Size = new System.Drawing.Size(86, 13);
            this.lbl_eur.TabIndex = 7;
            this.lbl_eur.Text = "Bedrag in euro\'s:";
            // 
            // rb_eurdol
            // 
            this.rb_eurdol.AutoSize = true;
            this.rb_eurdol.Location = new System.Drawing.Point(12, 102);
            this.rb_eurdol.Name = "rb_eurdol";
            this.rb_eurdol.Size = new System.Drawing.Size(89, 17);
            this.rb_eurdol.TabIndex = 8;
            this.rb_eurdol.TabStop = true;
            this.rb_eurdol.Text = "Euro -> Dollar";
            this.rb_eurdol.UseVisualStyleBackColor = true;
            this.rb_eurdol.CheckedChanged += new System.EventHandler(this.rb_eurdol_CheckedChanged);
            // 
            // rb_doleur
            // 
            this.rb_doleur.AutoSize = true;
            this.rb_doleur.Location = new System.Drawing.Point(183, 102);
            this.rb_doleur.Name = "rb_doleur";
            this.rb_doleur.Size = new System.Drawing.Size(89, 17);
            this.rb_doleur.TabIndex = 9;
            this.rb_doleur.TabStop = true;
            this.rb_doleur.Text = "Dollar -> Euro";
            this.rb_doleur.UseVisualStyleBackColor = true;
            this.rb_doleur.CheckedChanged += new System.EventHandler(this.rb_doleur_CheckedChanged);
            // 
            // lbl_err
            // 
            this.lbl_err.AutoSize = true;
            this.lbl_err.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_err.Location = new System.Drawing.Point(3, 192);
            this.lbl_err.Name = "lbl_err";
            this.lbl_err.Size = new System.Drawing.Size(0, 13);
            this.lbl_err.TabIndex = 10;
            // 
            // txt_cusval
            // 
            this.txt_cusval.Location = new System.Drawing.Point(179, 154);
            this.txt_cusval.Name = "txt_cusval";
            this.txt_cusval.Size = new System.Drawing.Size(100, 20);
            this.txt_cusval.TabIndex = 11;
            // 
            // chk_cusval
            // 
            this.chk_cusval.AutoSize = true;
            this.chk_cusval.Location = new System.Drawing.Point(6, 157);
            this.chk_cusval.Name = "chk_cusval";
            this.chk_cusval.Size = new System.Drawing.Size(167, 17);
            this.chk_cusval.TabIndex = 12;
            this.chk_cusval.Text = "Eigen wisselkoers (Eur/Dollar)";
            this.chk_cusval.UseVisualStyleBackColor = true;
            this.chk_cusval.CheckedChanged += new System.EventHandler(this.chk_cusval_CheckedChanged);
            // 
            // Valuta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.chk_cusval);
            this.Controls.Add(this.txt_cusval);
            this.Controls.Add(this.lbl_err);
            this.Controls.Add(this.rb_doleur);
            this.Controls.Add(this.rb_eurdol);
            this.Controls.Add(this.lbl_eur);
            this.Controls.Add(this.lbl_dol);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.btn_cusval);
            this.Controls.Add(this.txt_dol);
            this.Controls.Add(this.txt_eur);
            this.Name = "Valuta";
            this.Text = "Valuta";
            this.Load += new System.EventHandler(this.Valuta_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_eur;
        private System.Windows.Forms.TextBox txt_dol;
        private System.Windows.Forms.Button btn_cusval;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label lbl_dol;
        private System.Windows.Forms.Label lbl_eur;
        private System.Windows.Forms.RadioButton rb_eurdol;
        private System.Windows.Forms.RadioButton rb_doleur;
        private System.Windows.Forms.Label lbl_err;
        private System.Windows.Forms.TextBox txt_cusval;
        private System.Windows.Forms.CheckBox chk_cusval;
    }
}