﻿using System;
using System.Windows.Forms;

namespace Opdrachten
{
    public partial class Circle : Form
    {
        public Circle()
        {
            InitializeComponent();
        }

        private void show_err(int num)
        {
            string t = "";

            switch(num)
            {
                case 1:
                    t = "ERROR: Invalid radius entered.";
                    break;

                default:
                    break;
            }

            lbl_err.Text = t;
        }

        private int check_all()
        {
            double a = double.NaN;
            try
            {
                a = double.Parse(txt_radius.Text.Replace('.', ','));
            }
            catch(Exception err)
            {
                show_err(1);
                return 1;
            }

            if(double.IsNaN(a))
            {
                show_err(1);
                return 1;
            }

            return 0;
        }

        private void Circle_Load(object sender, EventArgs e)
        {
            txt_diameter.ReadOnly = true;
            txt_area.ReadOnly = true;
            txt_circumference.ReadOnly = true;
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int res = check_all();
            if (res == 0)
            {
                double radius = double.Parse(txt_radius.Text.Replace('.', ','));

                double diameter = radius * 2;
                double area = Math.PI * Math.Pow(radius, 2);
                double circumference = 2 * Math.PI * radius;

                txt_diameter.Text = diameter.ToString();
                txt_area.Text = area.ToString();
                txt_circumference.Text = circumference.ToString();
            }
        }
    }
}
