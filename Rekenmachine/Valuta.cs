﻿using System;
using System.Windows.Forms;

namespace Opdrachten
{
    public partial class Valuta : Form
    {
        private double eurtodol;

        public Valuta()
        {
            InitializeComponent();
        }

        private void show_err(int type = 0)
        {
            string t = "";

            switch (type)
            {
                case 1:
                    t = "ERROR: No such thing as negative money.";
                    break;

                case 2:
                    t = "ERROR: Not a number entered.";
                    break;

                case 3:
                    t = "ERROR: Conversion not selected.";
                    break;

                case 4:
                    t = "ERROR: Custom valuta not a number.";
                    break;

                default:
                    break;
            }

            lbl_err.Text = t;
        }

        private int run_checks()
        {
            bool eurdol = rb_eurdol.Checked;
            bool doleur = rb_doleur.Checked;

            if (!eurdol && !doleur)
            {
                show_err(3);
                return 3;
            }

            double e = double.NaN;
            double d = double.NaN;

            if(rb_eurdol.Checked)
            {
                try
                {
                    e = double.Parse(txt_eur.Text);
                    if(double.IsNaN(e))
                    {
                        show_err(2);
                        return 2;
                    }
                }
                catch(Exception err)
                {
                    show_err(2);
                    return 2;
                }
            }
            else
            {
                try
                {
                    d = double.Parse(txt_dol.Text);
                    if(double.IsNaN(d))
                    {
                        show_err(2);
                        return 2;
                    }
                }
                catch(Exception err)
                {
                    show_err(2);
                    return 2;
                }
            }

            if(double.IsNaN(d))
            {
                if(e < 0)
                {
                    show_err(1);
                    return 1;
                }
            }
            else
            {
                if(d < 0)
                {
                    show_err(1);
                    return 1;
                }
            }

            show_err(0);
            return 0;
        }

        private string reverse(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        private string parse_money(double mon)
        {
            string[] ret_wd = mon.ToString().Replace(".", "").Split(',');

            string ret_rev = reverse(ret_wd[0]);

            int char_count = 0;
            for(int i = 0; i < ret_rev.Length; i++)
            {
                if(char_count == 3 && i != ret_wd[0].Length - 1)
                {
                    ret_rev = ret_rev.Insert(i, ".");
                    char_count = 0;
                }
                else
                {
                    char_count += 1;
                }
            }

            try
            {
                return reverse(ret_rev) + "," + ret_wd[1].PadLeft(2, '0');
            }
            catch(Exception e)
            {
                return reverse(ret_rev) + ",00";
            }
        }

        private void Valuta_Load(object sender, EventArgs e)
        {
            txt_eur.ReadOnly = true;
            txt_dol.ReadOnly = true;
            txt_cusval.ReadOnly = true;
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int err = run_checks();
            if (err == 0)
            {
                bool eurdol = rb_eurdol.Checked;
                if (eurdol)
                {
                    double r = Math.Round(eurtodol * double.Parse(txt_eur.Text.Replace('.', ',')), 2);
                    txt_dol.Text = "$" + parse_money(r);
                    txt_dol.ReadOnly = true;
                }
                else
                {
                    double r = Math.Round(double.Parse(txt_dol.Text.Replace('.', ',')) / eurtodol, 2);
                    txt_eur.Text = "€" + parse_money(r);
                    txt_eur.ReadOnly = true;
                }
            }
        }

        private void rb_eurdol_CheckedChanged(object sender, EventArgs e)
        {
            txt_eur.ReadOnly = false;
            txt_dol.ReadOnly = true;

            txt_eur.Text = "";
            txt_dol.Text = "";
        }

        private void rb_doleur_CheckedChanged(object sender, EventArgs e)
        {
            txt_eur.ReadOnly = true;
            txt_dol.ReadOnly = false;

            txt_eur.Text = "";
            txt_dol.Text = "";
        }

        private void chk_cusval_CheckedChanged(object sender, EventArgs e)
        {
            if(chk_cusval.Checked)
            {
                txt_cusval.ReadOnly = false;
                eurtodol = 1.07d;
            }
            else
            {
                txt_cusval.Text = "";
                txt_cusval.ReadOnly = true;
            }
        }

        private void btn_cusval_Click(object sender, EventArgs e)
        {
            double a = double.NaN;

            try
            {
                a = double.Parse(txt_cusval.Text.Replace('.', ','));
            }
            catch(Exception err)
            {
                show_err(4);
                eurtodol = 1.07d;
            }

            if(!double.IsNaN(a))
            {
                eurtodol = a;
                show_err(0);
            }
            else
            {
                show_err(4);
                eurtodol = 1.07d;
            }
        }
    }
}
