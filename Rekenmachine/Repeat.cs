﻿using System;
using System.Windows.Forms;

namespace Opdrachten
{
    public partial class Repeat : Form
    {
        public Repeat()
        {
            InitializeComponent();
        }

        private void show_err(int num)
        {
            string t = "";
            switch (num)
            {
                case 1:
                    t = "ERROR: Nothing to repeat.";
                    break;

                case 2:
                    t = "ERROR: Cannot repeat non-number of times.";
                    break;

                default:
                    break;
            }

            lbl_err.Text = t;
        }

        private int check_all()
        {
            if(string.IsNullOrEmpty(txt_repeat.Text))
            {
                show_err(1);
                return 1;
            }

            int? a = null;
            try
            {
                a = int.Parse(txt_count.Text.Replace('.', ','));
            }
            catch(Exception err)
            {
                show_err(2);
                return 2;
            }

            return 0;
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            int res = check_all();
            if (res == 0)
            {
                string t = txt_repeat.Text;
                int count = int.Parse(txt_count.Text.Replace('.', ','));

                for (int i = 0; i < count; i++)
                {
                    MessageBox.Show(t, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
