# README #

### How to get setup with this repository ###
1. Clone this repository with git
    1. Download and install git from https://git-scm.com/
    2. Open the newly installed Git Bash application.
    3. Navigate to the directory where you wish to clone this (using cd <newdirectory>)
    4. Clone the repository (git clone <link shown above>)
2. Open the desired .sln file with your editor of choice.